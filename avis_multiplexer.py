"""
This is a part of AVIS project, which is combining distance sensors (VL53L0X) with a multiplexer (TCA9548A).


VL53L0X:
SPDX-FileCopyrightText: 2021 Smankusors for Adafruit Industries
SPDX-License-Identifier: MIT

"""

# /usr/bin/env python3
import time
import board
import logging
import busio
import adafruit_vl53l0x
from smbus import SMBus


# selected i2c channel on rpi
I2C_CHANNEL = 1

class tca9548A(object):

	def __init__(self, address=0x70):
		
		self._address = address #Multiplexer'address default
		self._bus = SMBus(I2C_CHANNEL)
		#self._activeID = []
		time.sleep(0.05)
		self.disable_mux()

		#self.ping_all()
		
	def select_device(self, ch_num):
		"""
		ch_num: Channel number 
		"""
		self.ch_num = ch_num
		selected_channel = 1 << self.ch_num
		self._bus.write_byte(self._address,selected_channel)
		#self._bus.write_byte_data(self._address, 0x04, selected_channel) # 0x04 is the register for switching channels

	def disable_mux(self):
		try:
			self._bus.write_byte(0x70, 0)
			self._bus.write_byte(0x71, 0)
		except:
			self._bus.write_byte(self._address, 0)
		print("Done disable")

	def ping_all(self):
		#To check available channels that operable with.

		for ch in range(0, 6):
			
			self.select_device(ch)
			try:		
				i2c = busio.I2C(board.SCL, board.SDA)
				vl53 = adafruit_vl53l0x.VL53L0X(i2c)

				# To store availble channels on the multiplexer.
				self._activeID.append(self.ch_num) 
				
			except:
				print("Emtry channel -> {}\n".format(ch))
			self._bus.write_byte(self._address, 0) #For disable multiplexer.

	def read_distance(self, measurement_timing=200000): #200000, 20000, 33000
		# Initialize I2C bus and sensor.
		#try:
		i2c = busio.I2C(board.SCL, board.SDA)
		vl53 = adafruit_vl53l0x.VL53L0X(i2c)
		vl53.measurement_timing_budget = measurement_timing
		time.sleep(0.05)

		range_ = vl53.range	
		self._bus.write_byte(self._address, 0) #For disable multiplexer.
		
		return range_
		'''except:
			self._bus.write_byte(self._address, 0)
			time.sleep(0.05)
			return 0 #The data from laser sensor will return atleast 30 mm.'''

	def main(self):
		resultList = []
		for ch in range(0, 6):
			self.select_device(ch)
			data = self.read_distance()
			time.sleep(0.05)	
			resultList.append(data) 
		self.disable_mux()

		return resultList	


if __name__ == "__main__":
	plexer = tca9548A(0x71).main()
	print("List: ", plexer)
	'''for activeID in plexer._activeID:
		plexer.select_device(activeID)
		data_ = plexer.read_distance()
		print("{}, Range1: {}".format(activeID, data_))
		time.sleep(0.01)'''

		





