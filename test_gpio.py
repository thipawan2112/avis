import RPi.GPIO as GPIO
import time

dischargetPin = 6


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(dischargetPin, GPIO.OUT, initial=GPIO.HIGH)

while True:
	GPIO.output(dischargetPin, GPIO.LOW)
	print("LOW\n")
	time.sleep(1)
	GPIO.output(dischargetPin, GPIO.HIGH)
	print("HIGH\n")
	time.sleep(1)
