#include <iostream>
#include <wiringPiI2C.h>

#define current_5V 0x40
#define current_12V 0x041

int main (int argc, char **argv){

	//Set up I2C communication
	int fd5 = wiringPiI2CSetup(current_5V);
	int fd12 = wiringPiI2CSetup(current_12V);

	if (fd5 == -1 || fd12 == -1){
		std::cout << "Failed to init I2C communication.\n";
		return -1;	
	}
	std::cout << "I2C communication successfully setup.\n";

	//Read data from current sensors
	int received_5v = wiringPiI2CRead(fd5);
	std::cout << "Data received 5 volt: " << received_5v << "\n";

	int received_12v = wiringPiI2CRead(fd12);
	std::cout << "Data received 12 volt: " << received_12v << "\n";
	return 0;
}

