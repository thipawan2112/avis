import smbus
import time
bus = smbus.SMBus(1)

#I2C address of arduino slave
address = 0x05

LEDst = [0x00,0x01,0x00,0x00,0x01,0x00,0x00,0x00,0x01]
for s in LEDst:
    bus.write_byte(address, s)
    print("Status: ", s)
    time.sleep(1)
