"""
This is a part of AVIS project, which is combining distance sensors (VL53L0X) with a multiplexer (TCA9548A).


VL53L0X:
SPDX-FileCopyrightText: 2021 Smankusors for Adafruit Industries
SPDX-License-Identifier: MIT

"""
import time
import board
import busio
import adafruit_vl53l0x
from smbus import SMBus

# selected i2c channel on rpi
I2C_CHANNEL = 1

class tca9548A(object):

	
	def __init__(self, address=0x70):
		self._address = address #Multiplexer'address default
		self._bus = SMBus(I2C_CHANNEL)

	def select_device(self, ch_num):
		"""
		ch_num: Channel number 
		"""

		selected_channel = 1 << ch_num
		print("selected_channel: ", selected_channel)
		self._bus.write_byte_data(self._address, 0x04, select_bus) # 0x04 is the register for switching channels

	def read_distance(self, measurement_timing=200000)
		# Initialize I2C bus and sensor.
		i2c = busio.I2C(board.SCL, board.SDA)
		vl53 = adafruit_vl53l0x.VL53L0X(i2c)
		vl53.measurement_timing_budget = measurement_timing
		time_ = time.time() - curTime
		range_ = vl53.range

		return range_, time_

if __name__ == "__main__":
	plexer = tca9548A()
	while True:
		plexer.select_device(2)
		data_, time_ = plexer.read_distance()
		print("2, Range: {}, time: {}\n".format(data_, time_)
		time.sleep(1)
		print("2, Range: {}, time: {}\n".format(data_, time_)
		time.sleep(1)

		plexer.select_device(5)
		data_, time_ = plexer.read_distance()
		print("5, Range: {}, time: {}\n".format(data_, time_)
		time.sleep(1)
		print("5, Range: {}, time: {}\n".format(data_, time_)
		time.sleep(1)



