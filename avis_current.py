from time import sleep
from ina219 import INA219

ina = INA219(shunt_ohms=0.1,
			max_expected_amps = 5,
			address=0x40)

ina.configure(voltage_range=ina.RANGE_32V,
			gain=ina.GAIN_AUTO,
			bus_adc=ina.ADC_128SAMP,
			shunt_adc=ina.ADC_128SAMP)

try:
	while 1:
		v = ina.voltage();
		i = ina.current()
		p = ina.power()
		print("\nvoltage: v\n", v)
		print("Current: i\n", i)
		sleep(1)

except KeyboardInterrupt:
	print("\nCtrl-c presssed.")
finally:
	pass
