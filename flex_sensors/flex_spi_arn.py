import spidev
import time

class flex_api:

	def __init__(self):
		spi_bus = 0
		spi_device = 0
		self.spi = spidev.SpiDev()
		self.spi.open(spi_bus, spi_device)
		self.spi.max_speed_hz = 1000000

	def main(self, position):
		'''Position: 
			0 = A flex sensor at the front.
			1 = A flex sensor at the rear.'''

		rcv_byte = self.spi.xfer2([0, position])
		time.sleep(1)
		data_rcv = rcv_byte[0]
		return data_rcv

if __name__ == "__main__":
	data = flex_api().main(0)
	print(data)
