
import spidev
spi = spidev.Spidev()
spi.open(0,0)

def readCh(ch):
	adc = spi.xfer2([6+((4&ch)>>2), (3&ch)<<6,0])
	data = ((adc[1]&15) <<8) + adc[2]
	return data

print("return: ", readCh(0))
