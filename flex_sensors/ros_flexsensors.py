#! /usr/bin/env python
#Import libraries

import rospy
from flexar_sensors import flexAR
from std_msgs.msg import Int32MultiArray

class flexSensors():

	def __init__(self):
		self.sensor_publisher = rospy.Publisher("/flexar_pub", Int32MultiArray, queue_size = 1)
		self.arrayResult = Int32MultiArray()
		self.ctrl_c = False #trigged when pressed ctrl_c
		self.rate = rospy.Rate(1) #2Hz
		rospy.on_shutdown(self.shutdownhook)
	
	def publish_cmd(self):
		while not self.ctrl_c:
			connections = self.sensor_publisher.get_num_connections()
			if connections > 0:
				self.sensor_publisher.publish(self.arrayResult)
				rospy.loginfo("Finished publishing!")
				break
			else:
				self.rate.sleep()
	def shutdownhook(self):
		self.ctrl_c = True
	
	def read_laser(self):
		#result = [self.frontLasers.main(), self.rearLasers.main()]'
		while not self.ctrl_c:
			#Call multiSensors initial
			self.frontLasers = flexAR(0).main()
			self.rate.sleep()
			result = [self.frontLasers]
			self.rearLasers = flexAR(1).main()
			self.rate.sleep()
			result.append(self.rearLasers)

			self.arrayResult.data = result
			rospy.loginfo("Reading flex AR sensors!")
			self.publish_cmd()
			self.rate.sleep()

if __name__ == "__main__":
	rospy.init_node("flexsensors_node", anonymous=True)
	laser_object = flexSensors()
	try:
		laser_object.read_laser()
	except rospy.ROSInterruptException:
		pass

		
