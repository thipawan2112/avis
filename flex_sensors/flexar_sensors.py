#!/usr/bin/env python

import RPi.GPIO as GPIO
import time
import spidev
import pigpio

import platform
import os

# ok, I don't want this to work in travis.ci env or on anything other than linux
try:
	if 'CI' in os.environ or platform.system() != 'Linux':
		raise ImportError()
	import Adafruit_GPIO.SPI as SPI
except ImportError:
	class SPI(object):
		MSBFIRST = 1
		class SpiDev(object):
			def __init__(self, a, b, max_speed_hz): pass
			def transfer(self, a): return [0x7f]*len(a)
			def set_mode(self, a): pass
			def set_bit_order(self, a): pass
			def close(self): pass


class MCP3208(object):
	def __init__(self):

		self.spi = SPI.SpiDev(0, 0, max_speed_hz=1000000)
		self.spi.set_mode(0)
		self.spi.set_bit_order(SPI.MSBFIRST)

	def __del__(self):
		self.spi.close()

	def read(self, ch):
		if 7 <= ch <= 0:
			raise Exception('MCP3208 channel must be 0-7: ' + str(ch))

		cmd = 128  # 1000 0000
		cmd += 64  # 1100 0000
		cmd += ((ch & 0x07) << 3)
		ret = self.spi.transfer([cmd, 0x0, 0x0])

		# get the 12b out of the return
		val = (ret[0] & 0x01) << 11  # only B11 is here
		val |= ret[1] << 3           # B10:B3
		val |= ret[2] >> 5           # MSB has B2:B0 ... need to move down to LSB

		return (val & 0x0FFF)  # ensure we are only sending 12b			

class flexAR():

	def __init__(self, position):
		'''Position is a param that refer the position of FlecAR, which are
		params: 
			0: mean the FlexAR at the front
				GPIO19 for generate pulses
				channel0 for ADC
				GPIO5 for discharging the capacitor
			1: mean the FlexAR at the rear
			 	GPIO13 for generate pulses
				channel1 for ADC
				GPIO6 for discharging the capacitor
		'''
		gpio_index = [[19, 0, 5], [13, 1, 6]]
		self.pi_gpio = pigpio.pi()
		self.adc = MCP3208()

		self.flexAR = gpio_index[position][0]
		self.pi_gpio.set_mode(self.flexAR, pigpio.OUTPUT)  # GPIO as input
		self.pi_gpio.hardware_PWM(self.flexAR, 0, 0) #50% of duty cycle

		self.adc_ch = gpio_index[position][1] 
		self.dischargetPin = gpio_index[position][2] 
		self.cs = 22

		GPIO.setwarnings(False)
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self.flexAR, GPIO.OUT)
		GPIO.setup(self.dischargetPin, GPIO.IN)

		GPIO.setup(self.cs, GPIO.OUT)
		GPIO.output(self.cs, GPIO.HIGH)

	def analog_read(self, channel):
		r = self.spi.xfer2([4 | 2 |(channel>>2), (channel &3) << 6,0])
		adc_out = ((r[1]&15) << 8) + r[2]
		return adc_out

	def applyPulses(self):
		self.pi_gpio.hardware_PWM(self.flexAR, 66000, 500000) #50% of duty cycle

	def disable(self):
		self.adc.__del__()
		self.pi_gpio.hardware_PWM(self.flexAR, 0, 0) #50% of duty cycle

	def main(self):
		try:
			GPIO.setup(self.dischargetPin, GPIO.OUT)
			GPIO.output(self.dischargetPin, GPIO.LOW)
			time.sleep(0.00001)
			GPIO.setup(self.dischargetPin, GPIO.IN)

			self.applyPulses()
			GPIO.output(self.cs, GPIO.LOW) #Pull chip select down to read data from the sensor	
			time.sleep(0.01)
			data = self.adc.read(self.adc_ch)
			#print("sensor{}: {}\n".format(self.adc_ch, data))
			GPIO.output(self.cs, GPIO.HIGH)
			self.pi_gpio.hardware_PWM(self.flexAR, 0, 0) #50% of duty cycle
			return data
			
		except KeyboardInterrupt:
			self.disable()
			print("Interrupted!")
			raise SystemExit

		
if __name__ == "__main__":
	flex = flexAR(1)
	flex.main()
	




