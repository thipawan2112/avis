#! /usr/bin/env python
import time
#import board
import logging
#import busio
#import adafruit_vl53l0x
import VL53L0X

class tca9548A():

	def __init__(self, address=0x70):
		self._address = address #Multiplexer'address default
		# Initialize I2C bus and sensor.
		self.laser = VL53L0X.VL53L0X(tca9548a_addr=self._address)
		
	def read_distance(self, ch): #200000, 20000, 33000 

		self.laser._tca9548a_num = ch
		self.laser.open()
		self.laser.start_ranging(VL53L0X.Vl53l0xAccuracyMode.HIGH_SPEED)
		'''self.timing = timing = self.laser.get_timing()
		if self.timing < 20000:
		    self.timing = 20000'''

		distance = self.laser.get_distance()
		#time.sleep(self.timing/1000000.00)

		self.laser.stop_ranging()
		return distance

	def disable_mux(self):	
		self.laser._i2c.write_byte(self._address, 0)
		self.laser.close()
		print("Done disable multiplexer!")

	def main(self):

		resultList = []
		for ch in range(6):		
			data = self.read_distance(ch)
			resultList.append(data)

		self.disable_mux()
		return resultList	


if __name__ == "__main__":
	plexer = tca9548A(0x70).main()
	plexer.close()
	print(plexer)
	plexer = tca9548A(0x71).main()
	print(plexer)

		





