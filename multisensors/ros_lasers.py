#!/usr/bin/env python
#Import libraries

import rospy
from multi_lasers import tca9548A
#from std_msgs.msg import UInt32MultiArray
from multisensors.msg import laser

class multiLaser():

	def __init__(self):
		self.sensor_publisher = rospy.Publisher("/laser_pub", laser, queue_size=1)
		self.arrayResult = laser()
		self.ctrl_c = False #trigged when pressed ctrl_c
		self.rate = rospy.Rate(10) #2Hz
		rospy.on_shutdown(self.shutdownhook)

	def publish_cmd(self):
		while not self.ctrl_c:
			connections = self.sensor_publisher.get_num_connections()
			if connections > 0:
				self.sensor_publisher.publish(self.arrayResult)
				rospy.loginfo("Finished publishing!")
				break
			else:
				self.rate.sleep()

	def shutdownhook(self):
		self.ctrl_c = True
	
	def read_laser(self):
		
		while not self.ctrl_c:
			frontLasers = tca9548A(0x70).main()
			rearLasers = tca9548A(0x71).main()
			#result = self.frontLasers
			self.arrayResult.frontdata = frontLasers
			self.arrayResult.raerdata = rearLasers
			rospy.loginfo("Reading laser sensors!")
			self.publish_cmd()	

if __name__ == "__main__":
	rospy.init_node("MultiLasers_node", anonymous=True)
	laser_object = multiLaser()
	try:
		laser_object.read_laser()
	except rospy.ROSInterruptException:
		pass

		
