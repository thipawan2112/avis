#! /usr/bin/env python3
import RPi.GPIO as GPIO
import time
#from mcp3208 import MCP3208
import spidev
import pigpio

class flexAR():

	def __init__(self, position):
		'''Position is a param that refer the position of FlecAR, which are
		params: 
			0: mean the FlexAR at the front
				GPIO19 for generate pulses
				channel0 for ADC
				GPIO5 for discharging the capacitor
			1: mean the FlexAR at the rear
			 	GPIO13 for generate pulses
				channel1 for ADC
				GPIO6 for discharging the capacitor
		'''
		gpio_index = [[19, 0, 5], [13, 1, 6]]

		self.flexAR = gpio_index[position][0] 
		self.adc_ch = gpio_index[position][1] 
		self.dischargetPin = gpio_index[position][2] 
		self.cs = 7 #22

		GPIO.setwarnings(False)
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self.flexAR, GPIO.OUT)
		GPIO.setup(self.dischargetPin, GPIO.IN)

		GPIO.setup(self.cs, GPIO.OUT)
		GPIO.output(self.cs, GPIO.HIGH)

		self.spi = spidev.SpiDev()
		self.spi.open(0,0)

		self.pi_gpio = pigpio.pi()
		self.pi_gpio.set_mode(self.flexAR, pigpio.ALT5) #pigpio.OUTPUT	
		self.disable()

	def analog_read(self, channel):
	    adc = self.spi.xfer2([ 6 | (channel&4) >> 2, (channel&3)<<6, 0])
	    data = ((adc[1]&3) << 8) + adc[2]
	    return data

	def readADC(self, ch):
		cmd = ((0x00 << 7) | (1 << 6) | ((ch & 0x07) <<3))
		adc = self.spi.xfer2([cmd, 0x00, 0x00])
		data = 0xFFF & ((adc[0] & 0x01) << 11 | (adc[1] & 0xFF) << 3 | (adc[2] & 0xE0) >> 5)
		return data

	def applyPulses(self):
		self.pi_gpio.hardware_PWM(self.flexAR,	80000, 500000) #50% of duty cycle

	def disable(self):
		self.pi_gpio.hardware_PWM(self.flexAR, 0, 0) #50% of duty cycle
		
	def main(self):
		GPIO.setup(self.dischargetPin, GPIO.OUT)
		GPIO.output(self.dischargetPin, GPIO.LOW)
		time.sleep(0.000010)
		GPIO.setup(self.dischargetPin, GPIO.IN)

		self.applyPulses()
		
		#GPIO.output(self.cs, GPIO.LOW) #Pull chip select down to read data from the sensor	
		data = self.analog_read(self.adc_ch)
		time.sleep(0.1)
		print("sensor{}: {}\n".format(self.adc_ch, data))
		self.disable()

if __name__ == "__main__":
	#0 the pulse signal wii one of them.  0 = rear, worked at ch 1!
	#1 the pulse signal wii open both ports.
	flex = flexAR()
	for i in range(100):
	#flex.disable()
		flex.main()
	flex.spi.close()
	




